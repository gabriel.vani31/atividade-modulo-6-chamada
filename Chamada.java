import java.util.ArrayList;
import java.util.HashMap;

public class Chamada {

    public static void main(String[] args) {
        ArrayList<String> listaAlunos = new ArrayList<>();
        listaAlunos.add("Gabriel");
        listaAlunos.add("Java");

        HashMap<String, String> map = new HashMap<>();
        map.put("01/03/2022", " Presente");
        map.put("02/03/2022", " Ausente");
        map.put("03/03/2022", " Presente");
        map.put("04/03/2022", " Ausente");
        map.put("05/03/2022", " Presente");
        map.put("06/03/2022", " Ausente");

        System.out.print(listaAlunos.get(0));
        System.out.println(map.get("01/03/2022"));
        System.out.print(listaAlunos.get(0));
        System.out.println(map.get("02/03/2022"));
        System.out.print(listaAlunos.get(0));
        System.out.println(map.get("03/03/2022"));
        System.out.print(listaAlunos.get(0));
        System.out.println(map.get("04/03/2022"));
        System.out.print(listaAlunos.get(0));
        System.out.println(map.get("05/03/2022"));
        System.out.print(listaAlunos.get(0));
        System.out.println(map.get("06/03/2022"));
        System.out.println(" ");
        System.out.print(listaAlunos.get(1));
        System.out.println(map.get("01/03/2022"));
        System.out.print(listaAlunos.get(1));
        System.out.println(map.get("02/03/2022"));
        System.out.print(listaAlunos.get(1));
        System.out.println(map.get("03/03/2022"));
        System.out.print(listaAlunos.get(1));
        System.out.println(map.get("04/03/2022"));
        System.out.print(listaAlunos.get(1));
        System.out.println(map.get("05/03/2022"));
        System.out.print(listaAlunos.get(1));
        System.out.println(map.get("06/03/2022"));
    }
}

